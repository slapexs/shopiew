<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script>
        const http = new XMLHttpRequest();
        const url = "action.php"
        const m_email = "<?php echo $_POST['m_email'];  ?>"
        const m_password = "<?php echo $_POST['m_password'];  ?>"
        const params = `m_email=${m_email}&m_password=${m_password}`
        http.open('POST', url, true)

        // Send header
        http.setRequestHeader('Content-type', "application/x-www-form-urlencoded")

        http.onreadystatechange = function() {
            if (http.readyState == 4 && http.status == 200) {
                const res_row = http.responseText
                if (res_row == 1) {
                    window.location.href = './backend/'
                } else {
                    alert("Email and password is invalid!")
                    window.location.href = "./login.php"
                }
            }
        }

        http.send(params)
    </script>
</head>

<body>
    <h1>Login</h1>
    <form action="login.php" method="post">
        <input type="email" name="m_email" id="m_email" placeholder="E-mail" required>
        <input type="password" name="m_password" id="m_password" placeholder="Password" required>
        <button type="submit" name="checklogin">Login</button>
    </form>
</body>

</html>